using System;

namespace GameServer.DTO
{
    public abstract class EntityBase  
    {  
        public Guid Id { get; set; }  
    }
}