namespace GameServer.DTO
{
    public class CommandModel
    {
        public string Action { get; set; }
        public CommandContextModel Context { get; set; }
    }
}