using System;

namespace CartageServer.DTO
{
    public abstract class EntityBase  
    {  
        public Guid Id { get; set; }  
    }
}